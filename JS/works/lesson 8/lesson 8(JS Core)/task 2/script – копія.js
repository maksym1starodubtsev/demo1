let MainCh = document.querySelectorAll('.main-ch');
let TextA = document.querySelector('#box');
let DELETE = document.querySelector('.delete');
let TAB = document.querySelector('.Tab');
let CAps = document.querySelector('.CapsLock');
let Shift = document.querySelector('.Shift');
let m = [];


let UPER = false;
let UPERSH = false;

for(const i of MainCh){
  i.setAttribute('onclick','change()');
}

DELETE.setAttribute('onclick', 'Delete()');
TAB.setAttribute('onclick', 'Tab()');
CAps.setAttribute('onclick', 'CAPs()');
Shift.setAttribute('keydown', 'SHift()');
Shift.setAttribute('keyup', 'UnSHift()');

function SHift(){
  if(UPERSH === false){
    UPERSH = true;
 }
}

function UnSHift(){
  UPERSH = false;
}

function CAPs(){
  if(UPER === false){
    UPER = true;
  }else{
    UPER = false;
  }
}

function change(){
  if(UPERSH === true){
    m.push(`${event.target.textContent.toUpperCase()}`);
  }else if(UPER === true){
    m.push(`${event.target.textContent.toUpperCase()}`);
  }else{
    m.push(`${event.target.textContent}`);
  }
  TextA.textContent = m.join('');
}

function Delete(){
  m.pop();
  TextA.textContent = m.join('');
}

function Tab(){
  m.push('        ');
  TextA.textContent = m.join('');
}


for(const i of MainCh){
  let q = i.textContent;
  let y = q.charCodeAt();
  // console.log(q.charCodeAt());//////////////////
  i.setAttribute('data', `${y}`);
}

window.addEventListener('keydown', (event) => {
  console.log(event);
  if (event.code !== 'Backspace') {
      document.querySelector('[data="'+ 8 +'"]').classList.remove('active');
  }
  if (event.code == 'Backspace') {
      // console.log(event);
      document.querySelector('[data="'+ 8 +'"]').classList.add('active');
      m.pop();
      TextA.textContent = m.join('');
  }
})

let MSc = document.querySelectorAll('.specific');

window.addEventListener('keydown', (event) => {

  for(const i of MSc){
    i.classList.remove('active');
  }

  for(const i of MSc){
    if(i.getAttribute('data') == event.keyCode){
      i.classList.add('active');
    };
  }
  
})


document.onkeypress = function (event) {
    if(event.keyCode == 13){
      return m.push('\n').join('');
    }
    m.push(`${event.key}`);
    TextA.textContent = m.join('');
    for(const i of MainCh){
      i.classList.remove('active',);
    };
    if(event.keyCode > 64 && event.keyCode < 91){
      document.querySelector('[data="'+ (event.keyCode + 32)+'"]').classList.add('active');
    }else{
      document.querySelector('[data="'+ event.keyCode +'"]').classList.add('active');
    }
  
}

window.addEventListener('keydown',function(event){
  if(event.keyCode == 9){
    event.preventDefault()
    m.push('      ');
  }
})
